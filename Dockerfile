FROM python

WORKDIR /app

COPY . .

RUN pip install poetry
RUN poetry install

CMD ["poetry", "run", "python", "server.py"]
